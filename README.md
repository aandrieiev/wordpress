# Deployment of a WordPress instance on Kubernetes

## Architecture
A classical 2-2-1 architecture is used: 2 caching reverse proxies (nginx), 2 app servers (wordpress + mod_php + apache2), and 1 DB (mysql) capable to handle moderate workload.
Publicly available images are used for nginx, mysql, and ubuntu bionic, atop of which are conseuqtanly built a base image with php v7.2 installed, and 
and the main app image with wordpress v.4.9.8 used as an examplary PHP application deployment setup:<br><br>

![Alt text](misc/diagram.png?raw=true "Diagram")
<br>Pic 1. Architecture diagram

## Details
### General

Each component is dockerized and run in 2 (proxy and app) or 1 (DB) replicas. In front of the proxies is a k8s loadbalancer service that exposes this setup to external access.
DB and proxy pods contain persisting volumes, DB's for business data, proxies' for storing cache.

### Images
Since Wordpress is used as an examplary application, it's source code is stored in the git repo. Separate repos (and CI builds) are set up for nginx and base images (see the projects one level up off this repo).
All images are stored in the GitLab Docker registry.

### CI Pipeline
The simplest pipeline schema is used: "Build and Push" -> "Deploy" -> "Smoke Test".
"Build and Push": build the Docker image from a Dockerile and push to the registry
"Deploy": pre-configure remote access to a k8s cluster and using locally available YAML manifest create respective deployments
"Smoke Test": run some smoke tests agains the deployed application to make sure it's at least available

### Local testing with Minikube
Ensure your Minikube cluster has access to the GitLab registry to pull images:<br>
`kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<password> --docker-email=<email>`

Set passwords that will be later passed to images in order to provision MySQL:<br>
`kubectl create secret generic mysql-root-pass --from-literal=password=<your_password>`<br>
`kubectl create secret generic mysql-wp-pass --from-literal=password=<your_password>`<br>

Once all set, run kubectl create:<br>
`cd manifest && kubectl -f mysql.yml -f wordpress.yml -f nginx.yml`

### Deployment
Since this configuration was tested only locally with Minikube, the actually deployment stage of CI is stubbed with no actual actions taken.
