FROM registry.gitlab.com/aandrieiev/php72-base:latest

COPY wordpress /var/www/html/wordpress/
COPY wordpress.conf /etc/apache2/sites-available/

RUN service apache2 start                                           &&\
    a2ensite wordpress.conf && a2enmod rewrite                      &&\
    a2dissite 000-default.conf && service apache2 reload            &&\
    service apache2 stop

CMD apachectl -D FOREGROUND
